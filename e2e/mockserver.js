//
// This server will be started by Protractor in end-to-end tests.
// Add your API mocks for your specific project in this file.
//
const express = require("express");
const port = 3000;

let app = express();
let routes = require("express").Router();

// Add CORS headers so our external Angular app is allowed to connect
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});

routes.post("/api/login", (req, res, next) => {
  res.status(200).json({
    name: "Firstname",
    lastName: "Lastname",
    email: "first.last@avans.nl",
    token:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1ZmNlYjUwNzhiNTM2YTEzOTQwMWQ0ZTUiLCJpYXQiOjE2MDc0MzkyMDEsImV4cCI6MTYwODA0NDAwMX0.FSV_s2SgkzPUg_wuIw1zDkZ58UucF5yOtWCsdao6HAg",
  });
});

//
// Write your own mocking API endpoints here.
//

// Finally add your routes to the app
app.use(routes);

app.use("*", function (req, res, next) {
  next({ error: "Non-existing endpoint" });
});

app.use((err, req, res, next) => {
  res.status(400).json(err);
});

app.listen(port, () => {
  console.log("Mock backend server running on port", port);
});
