import { by, element, ElementFinder } from 'protractor';
import { CommonPageObject } from '../../common.po';

export class MovieEditPage extends CommonPageObject {
  get movieButton(): ElementFinder {
    return element.all((by.id('movie-button'))).first() as ElementFinder;
  }

  get name(): ElementFinder {
    return element(by.id('name')) as ElementFinder;
  }

  get genre(): ElementFinder {
    return element(by.id('genre')) as ElementFinder;
  }

  get age(): ElementFinder {
    return element(by.id('age')) as ElementFinder;
  }

  get releaseDate(): ElementFinder {
    return element(by.id('releaseDate')) as ElementFinder;
  }

  get desc(): ElementFinder {
    return element(by.id('desc')) as ElementFinder;
  }

  get image(): ElementFinder {
    return element(by.id('image')) as ElementFinder;
  }

  get returnButton(): ElementFinder {
    return element(by.id('return-button')) as ElementFinder;
  }

  get submit() {
    return element(by.id("movie-submit")) as ElementFinder;
  }
}
