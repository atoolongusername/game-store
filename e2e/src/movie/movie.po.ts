import { by, element, ElementFinder } from 'protractor';
import { CommonPageObject } from '../../common.po';

export class MovieDetailPage extends CommonPageObject {
  get image(): ElementFinder {
    return element(by.id('image')) as ElementFinder;
  }

  get titleText(): ElementFinder {
    return element(by.id('title')) as ElementFinder;
  }

  get genreText(): ElementFinder {
    return element(by.id('genre')) as ElementFinder;
  }

  get ageText(): ElementFinder {
    return element(by.id('age')) as ElementFinder;
  }

  get releaseDateText(): ElementFinder {
    return element(by.id('release-date')) as ElementFinder;
  }

  get createdByText(): ElementFinder {
    return element(by.id('created-by')) as ElementFinder;
  }

  get updatedByText(): ElementFinder {
    return element(by.id('updated-by')) as ElementFinder;
  }

  get descText(): ElementFinder {
    return element(by.id('desc')) as ElementFinder;
  }

  get returnButton(): ElementFinder {
    return element(by.id('return-button')) as ElementFinder;
  }
}
