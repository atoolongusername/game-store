import { MovieDetailPage } from './movie.po';
import {browser, logging, element, by, protractor} from 'protractor';
import {LoginPage} from "./login.po";
import {MovieEditPage} from "./movie-edit.po";

describe('movie detail page', () => {
  let page: MovieDetailPage;

  beforeEach(async() => {
    page = new MovieDetailPage();
    await page.navigateTo('movies/detail/6080985935354b08e48c9a68');
  });

  it('should be at /movies/detail/6080985935354b08e48c9a68 route after initialisation', async() => {
    expect(await browser.driver.getCurrentUrl()).toContain('movies/detail/6080985935354b08e48c9a68');
  });

  it("should display the correct title text.", async() => {
    expect(await page.titleText.getText()).toContain("Bond, james bond");
  });

  it("should display the correct genre text.", async() => {
    expect(await page.genreText.getText()).toContain("Actie");
  });

  it("should display the correct age text.", async() => {
    expect(await page.ageText.getText()).toContain("18");
  });

  it("should display the correct release date text.", async() => {
    expect(await page.releaseDateText.getText()).toContain("");
  });

  it("should display the correct created by text.", async() => {
    expect(await page.createdByText.getText()).toContain("iii");
  });

  it("should display the correct updated by text.", async() => {
    expect(await page.updatedByText.getText()).toContain("5fd22c2b0682da002463f0d4");
  });

  it("should display the correct desc text.", async() => {
    expect(await page.descText.getText()).toContain("lalalalala");
  });

  it("should display the correct return button.", async() => {
    expect(await page.returnButton.getText()).toContain("Terug");
  });

  it("should display the correct image.", async() => {
    expect(await page.image.getAttribute("src")).toContain("assets/img/placeholder.png");
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});

describe("Movie Update Page", () => {
  let page: MovieEditPage;
  let login: LoginPage;

  beforeEach(async() => {
    page = new MovieEditPage();
    login = new LoginPage();

    browser.waitForAngularEnabled(true);

    browser.waitForAngularEnabled(true);
    await page.navigateTo("/movies/5fc4dec1a8328a4f501c18fa/edit");
    await login.email.sendKeys("test@test.nl");
    await login.password.sendKeys("testtest");
    await login.submit.click();
    await page.movieButton.click();
  });

  it("should be at movies/5fc4dec1a8328a4f501c18fa/edit route after initialisation", async() => {
      expect(await browser.driver.getCurrentUrl()).toContain("movies/5fc4dec1a8328a4f501c18fa/edit");
  });

  it("should display the correct name field.", async() => {
    expect(await page.name.isPresent()).toBe(true);
  });

  it("should display the correct genre field.", async() => {
    expect(await page.genre.isPresent()).toBe(true);
  });

  it("should display the correct age field.", async() => {
    expect(await page.age.isPresent()).toBe(true);
  });

  it("should display the correct releaseDate field.", async() => {
    expect(await page.releaseDate.isPresent()).toBe(true);
  });

  it("should display the correct desc field.", async() => {
    expect(await page.desc.isPresent()).toBe(true);
  });

  it("should display the correct image field.", async() => {
    expect(await page.image.isPresent()).toBe(true);
  });

  it("should display the correct submit button", async() => {
    expect(await page.submit.isPresent()).toBe(true);
  });

  it("should display the correct return button", async() => {
    expect(await page.returnButton.isPresent()).toBe(true);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});

function delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}
