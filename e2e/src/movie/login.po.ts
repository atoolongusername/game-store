import { by, element, ElementFinder } from 'protractor';
import { CommonPageObject } from '../../common.po';

export class LoginPage extends CommonPageObject {
  get loginButton() {
    return element(by.id("login-button")) as ElementFinder;
  }

  get email() {
    return element(by.id("email")) as ElementFinder;
  }

  get password() {
    return element(by.id("password")) as ElementFinder;
  }

  get submit() {
    return element(by.id("login-submit")) as ElementFinder;
  }
}
