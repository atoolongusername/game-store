import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { delay, switchMap, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/shared/auth.service';
import { Movie } from '../movie/movie.model';
import { MovieService } from '../movie/movie.service';
import { User } from '../user/user.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  movies$: Observable<Movie[]>;
  loggedInUser$ = new Observable<User | undefined>();

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute,
    private authService: AuthService
    ) { }

  ngOnInit(): void {
    this.loggedInUser$ = this.authService.currentUser$;

    this.movies$ = this.route.paramMap.pipe(
      delay(500),
      switchMap(() =>
        this.movieService.list()
      ),
      tap(console.log)
    );
  }

}
