import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { ShowListComponent } from './show-list.component';
import { ShowService } from "../show.service";
import {RouterTestingModule} from "@angular/router/testing";
import {Component, NO_ERRORS_SCHEMA} from "@angular/core";

@Component({ selector: 'app-loading', template: '' })
class LoadingComponent {}

@Component({ selector: 'app-alert', template: '' })
class AlertComponent {}

@Component({ selector: 'app-navbar', template: '' })
class NavbarComponent {}

@Component({ selector: 'app-footer', template: '' })
class FooterComponent {}

describe('ShowListComponent', () => {
  let component: ShowListComponent;
  let fixture: ComponentFixture<ShowListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowListComponent ],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [ShowService],
      schemas: [ NO_ERRORS_SCHEMA ],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

