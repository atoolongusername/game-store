import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { delay, switchMap, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/shared/auth.service';
import { User } from '../../user/user.model';
import { Show } from '../show.model';
import { ShowService } from '../show.service';

@Component({
  selector: 'app-show-list',
  templateUrl: './show-list.component.html',
  styleUrls: ['./show-list.component.css']
})
export class ShowListComponent implements OnInit {
  shows$: Observable<Show[]>;
  loggedInUser$ = new Observable<User | undefined>();

  constructor(
    private showService: ShowService,
    private route: ActivatedRoute,
    private authService: AuthService
    ) { }

  ngOnInit(): void {
    this.loggedInUser$ = this.authService.currentUser$;

    this.shows$ = this.route.paramMap.pipe(
      delay(500),
      switchMap(() =>
        this.showService.list()
      ),
      tap(console.log)
    );
  }

}
