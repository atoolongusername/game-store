import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ShowListComponent } from './show-list/show-list.component';
import { ShowDetailComponent } from './show-detail/show-detail.component';
import { ShowEditComponent } from './show-edit/show-edit.component';
import {LoadingModule} from "../../shared/loading/loading.module";

const routes: Routes = [
  { path: 'shows', pathMatch: 'full', component: ShowListComponent },
  { path: 'shows/detail/:id', pathMatch: 'full', component: ShowDetailComponent },
];

@NgModule({
  declarations: [ShowListComponent, ShowDetailComponent, ShowEditComponent],
  imports: [
    CommonModule, RouterModule.forRoot(routes), LoadingModule
  ],
})
export class ShowModule { }
