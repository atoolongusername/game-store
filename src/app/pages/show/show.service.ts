import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertService } from 'src/app/shared/alert/alert.service';
import { EntityService } from 'src/app/shared/common/entity-service.model';
import { environment } from 'src/environments/environment';
import { Show } from './show.model';

@Injectable({
  providedIn: 'root'
})
export class ShowService extends EntityService<Show> {
  shows: Show[];

  constructor(http: HttpClient, public alertService: AlertService) {
    super(http, environment.SERVER_API_URL, 'shows', alertService)
   }
}
