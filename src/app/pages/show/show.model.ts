import { Entity } from 'src/app/shared/common/entity.model';
import {Room} from "../room/room.model";
import {Movie} from "../movie/movie.model";

export class Show extends Entity {
  roomId: number;
  movieId: number;
  date: string;
  room: Room;
  movie: Movie;
}
