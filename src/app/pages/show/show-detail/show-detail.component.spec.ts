import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoadingModule } from 'src/app/shared/loading/loading.module';

import { ShowDetailComponent } from './show-detail.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {Component, NO_ERRORS_SCHEMA} from "@angular/core";

@Component({ selector: 'app-loading', template: '' })
class LoadingComponent {}

@Component({ selector: 'app-alert', template: '' })
class AlertComponent {}

@Component({ selector: 'app-navbar', template: '' })
class NavbarComponent {}

@Component({ selector: 'app-footer', template: '' })
class FooterComponent {}

describe('ShowDetailComponent', () => {
  let component: ShowDetailComponent;
  let fixture: ComponentFixture<ShowDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ LoadingModule, HttpClientTestingModule, RouterTestingModule ],
      declarations: [ ShowDetailComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
