import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { delay, switchMap, tap } from 'rxjs/operators';
import { Show } from '../show.model';
import { ShowService } from '../show.service';

@Component({
  selector: 'app-show-detail',
  templateUrl: './show-detail.component.html',
  styleUrls: ['./show-detail.component.css']
})
export class ShowDetailComponent implements OnInit {
  show: Show;
  sub: Subscription;

  constructor(private showService: ShowService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.sub = this.route.paramMap.pipe(
      delay(500),
      tap((params: ParamMap) => console.log('id =', params.get('id'))),
      switchMap((params: ParamMap) => {
        return this.showService.read(undefined, params.get('id') || '-1');
      }),
      tap(console.log)
    ).subscribe((show) => {
      this.show = show;
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
