import { Entity } from 'src/app/shared/common/entity.model';

export class User extends Entity {
  email: string;
  password: string
  name: string
  lastName: string
  birthdate: string
  phone: string
  isAdmin: boolean
  token: string
  friends?: string[]
}
