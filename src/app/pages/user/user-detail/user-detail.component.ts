import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { delay, switchMap, tap } from 'rxjs/operators';
import { AlertService } from 'src/app/shared/alert/alert.service';
import { AuthService } from 'src/app/shared/auth.service';
import { Ticket } from '../../ticket/ticket.model';
import { TicketService } from '../../ticket/ticket.service';
import { User } from '../user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  user: User;
  tickets: Ticket[];
  currentUser: User;
  sub: Subscription;
  subTickets: Subscription;
  currentUserId?: String;
  subCurrentUser: Subscription;
  loggedInUser$ = new Observable<User | undefined>();

  private dataList: Ticket[] = [];

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private ticketService: TicketService,
    private alertService: AlertService,
    ) { }

  ngOnInit(): void {
    this.sub = this.route.paramMap.pipe(
      delay(500),
      tap((params: ParamMap) => console.log('id =', params.get('id'))),
      switchMap((params: ParamMap) => {
        return this.userService.read(undefined, params.get('id') || '-1');
      }),
      tap(console.log)
    ).subscribe((user) => {
      this.user = user;
      this.user.birthdate = formatDate(user.birthdate, 'dd-MM-yyyy hh:mm', 'en-US');
    });

    this.loggedInUser$ = this.authService.currentUser$;
    this.subCurrentUser = this.loggedInUser$.subscribe((currentUser) => {this.currentUserId = currentUser?._id; this.currentUser = currentUser!; });

    //Tickets
    this.subTickets = this.route.paramMap.pipe(
      delay(500),
      tap((params: ParamMap) => console.log('userId =', '1')),
      switchMap(() =>
        this.ticketService.list()
      ),
      tap(console.log)
    ).subscribe((tickets) => {
      this.tickets = tickets;

      this.currentUser.friends?.forEach((friend) => {
        for(let i = 0; i < this.tickets.length; i++){
          if(this.tickets[i].userId === friend) {
            this.dataList.push(this.tickets[i]);
          }
        }
      })
      this.tickets = this.dataList;
    });
  }

  follow(): void {
    console.log('Follow request', `User #${this.currentUserId} is going to follow ${this.user.name} ${this.user.lastName}(${this.user._id})`);

    if(this.currentUser.friends?.find(x => x.toString() === this.user._id))
    {
      this.alertService.error('U volgt deze persoon al.');
      return;
    }

    this.authService
      .follow(this.user._id)
      .subscribe(() => {
          this.alertService.info(`Now following ${this.user.name} ${this.user.lastName}(${this.user._id})`);
        }
      );
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
    this.subTickets.unsubscribe();
    this.subCurrentUser.unsubscribe();
  }

}
