import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDeleteComponent } from './user-delete.component';
import {Component, NO_ERRORS_SCHEMA} from "@angular/core";

@Component({ selector: 'app-loading', template: '' })
class LoadingComponent {}

@Component({ selector: 'app-alert', template: '' })
class AlertComponent {}

@Component({ selector: 'app-navbar', template: '' })
class NavbarComponent {}

@Component({ selector: 'app-footer', template: '' })
class FooterComponent {}

describe('UserDeleteComponent', () => {
  let component: UserDeleteComponent;
  let fixture: ComponentFixture<UserDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserDeleteComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
