import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserDeleteComponent } from './user-delete/user-delete.component';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from 'src/app/shared/authguard/auth.guard';
import {LoadingModule} from "../../shared/loading/loading.module";

const routes: Routes = [
  { path: 'users', pathMatch: 'full', component: UserListComponent, canActivate: [AuthGuard] },
  { path: 'users/detail/:id', pathMatch: 'full', component: UserDetailComponent, canActivate: [AuthGuard] },
  { path: 'users/:id/edit', pathMatch: 'full', component: UserEditComponent, canActivate: [AuthGuard] },
  { path: 'users/:id/delete', pathMatch: 'full', component: UserDeleteComponent, canActivate: [AuthGuard] },
];

@NgModule({
  declarations: [UserListComponent, UserDetailComponent, UserEditComponent, UserDeleteComponent],
  imports: [
    CommonModule, RouterModule.forRoot(routes), FormsModule, LoadingModule
  ]
})
export class UserModule { }
