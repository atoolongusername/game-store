import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { delay, switchMap, tap } from 'rxjs/operators';
import { User } from '../user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users$: Observable<User[]>;
  usersRows: any[];

  constructor(private userService: UserService, private route: ActivatedRoute) { }

  	ngOnInit(): void {
    this.users$ = this.route.paramMap.pipe(
      delay(500),
      switchMap(() =>
        this.userService.list()
      ),
      tap(console.log)
    );
  }

}
