import { Component, OnInit } from '@angular/core'
import { UseCase } from './usecase.model'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker'
  readonly ADMIN_USER = 'Administrator'

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op de login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen.',
      postcondition: 'De actor is ingelogd.'
    },
    {
      id: 'UC-02',
      name: 'Uitloggen',
      description: 'Hiermee logt een bestaande gebruiker uit.',
      scenario: ['Gebruiker klikt op de logout knop',
      'De applicatie logt de gebruiker uit',
      'Redirect naar de hoofdpagina'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd.',
      postcondition: 'De actor is uitgelogd.'
    },
    {
      id: 'UC-03',
      name: 'Registreren',
      description: 'Hiermee maakt een nog niet ingelogde gebruiker een nieuw reguliere account aan.',
      scenario: ['Gebruiker vult gegevens in en klikt op de submit knop.',
      'De applicatie valideert de ingevoerde gegevens.',
      'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm en logt de gebruiker automatisch in.'],
      actor: this.PLAIN_USER,
      precondition: 'Geen.',
      postcondition: 'Een nieuw reguliere account is aangemaakt en de actor is ingelogd.'
    },
    {
      id: 'UC-04',
      name: 'Zaal aanmaken',
      description: 'Hiermee maakt een admin een nieuwe zaal aan.',
      scenario: ['Actor vult gegevens in en klikt op de submit knop.',
      'De applicatie valideert de ingevoerde gegevens.',
      'Indien gegevens correct zijn dan redirect de applicatie naar de index pagina.'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd.',
      postcondition: 'Een nieuwe zaal met de bijbehorende zetels is aangemaakt.'
    },
    {
      id: 'UC-05',
      name: 'Film aanmaken',
      description: 'Hiermee maakt een admin een nieuwe film aan.',
      scenario: ['Actor vult gegevens in en klikt op de submit knop.',
      'De applicatie valideert de ingevoerde gegevens.',
      'Indien gegevens correct zijn dan redirect de applicatie naar de index pagina.'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd.',
      postcondition: 'Een nieuwe film is aangemaakt.'
    },
    {
      id: 'UC-06',
      name: 'Voorstelling aanmaken',
      description: 'Hiermee maakt een admin een nieuwe show aan.',
      scenario: ['Actor vult gegevens in en klikt op de submit knop.',
      'De applicatie valideert de ingevoerde gegevens.',
      'Indien gegevens correct zijn dan redirect de applicatie naar de index pagina.'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en minimaal 1 zaal en film bestaan.',
      postcondition: 'Een nieuwe show is aangemaakt.'
    },
    {
      id: 'UC-07',
      name: 'Ticket aanmaken',
      description: 'Hiermee maakt een gebruiker een nieuwe ticket aan.',
      scenario: ['Actor vult gegevens in en klikt op de submit knop.',
      'De applicatie valideert de ingevoerde gegevens.',
      'Indien gegevens correct zijn dan redirect de applicatie naar de index pagina.'],
      actor: this.PLAIN_USER,
      precondition: 'De actor is ingelogd en minimaal 1 zetel in een zaal is vrij van een bestaande show.',
      postcondition: 'Een nieuwe ticket is aangemaakt.'
    },
    {
      id: 'UC-08',
      name: 'Gebruiker bewerken',
      description: 'Hiermee bewerkt een admin een gebruiker.',
      scenario: ['Actor vult gegevens in en klikt op de submit knop.',
      'De applicatie valideert de ingevoerde gegevens.',
      'Indien gegevens correct zijn dan redirect de applicatie naar de index pagina.'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en minimaal 1 gebruiker bestaat.',
      postcondition: 'De gebruiker is bewerkt.'
    },
    {
      id: 'UC-09',
      name: 'Zaal bewerken',
      description: 'Hiermee bewerkt een admin een zaal.',
      scenario: ['Actor vult gegevens in en klikt op de submit knop.',
      'De applicatie valideert de ingevoerde gegevens.',
      'Indien gegevens correct zijn dan redirect de applicatie naar de index pagina.'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en minimaal 1 zaal bestaat.',
      postcondition: 'De zaal is bewerkt.'
    },
    {
      id: 'UC-10',
      name: 'Film bewerken',
      description: 'Hiermee bewerkt een admin een film.',
      scenario: ['Actor vult gegevens in en klikt op de submit knop.',
      'De applicatie valideert de ingevoerde gegevens.',
      'Indien gegevens correct zijn dan redirect de applicatie naar de index pagina.'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en minimaal 1 film bestaat.',
      postcondition: 'De film is bewerkt.'
    },
    {
      id: 'UC-11',
      name: 'Voorstelling bewerken',
      description: 'Hiermee bewerkt een admin een voorstelling.',
      scenario: ['Actor vult gegevens in en klikt op de submit knop.',
      'De applicatie valideert de ingevoerde gegevens.',
      'Indien gegevens correct zijn dan redirect de applicatie naar de index pagina.'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en minimaal 1 voorstelling bestaat.',
      postcondition: 'De voorstelling is bewerkt.'
    },
    {
      id: 'UC-12',
      name: 'Ticket bewerken',
      description: 'Hiermee bewerkt een admin een ticket.',
      scenario: ['Actor vult gegevens in en klikt op de submit knop.',
      'De applicatie valideert de ingevoerde gegevens.',
      'Indien gegevens correct zijn dan redirect de applicatie naar de index pagina.'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en minimaal 1 ticket bestaat.',
      postcondition: 'De ticket is bewerkt.'
    },
    {
      id: 'UC-13',
      name: 'Gebruiker verwijderen',
      description: 'Hiermee verwijderd een admin een gebruiker.',
      scenario: ['Actor klikt op de verwijder knop.',
      'De applicatie geeft waarschuwingsmelding.',
      'Indien geaccepteerd dan redirect de applicatie naar de index pagina en de gebruiker wordt verwijderd.'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en minimaal 1 gebruiker bestaat.',
      postcondition: 'De gebruiker is verwijderd.'
    },
    {
      id: 'UC-14',
      name: 'Zaal verwijderen',
      description: 'Hiermee verwijderd een admin een zaal.',
      scenario: ['Actor klikt op de verwijder knop.',
      'De applicatie geeft waarschuwingsmelding.',
      'Indien geaccepteerd dan redirect de applicatie naar de index pagina en de zaal wordt verwijderd.'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en minimaal 1 zaal bestaat.',
      postcondition: 'De zaal is verwijderd.'
    },
    {
      id: 'UC-15',
      name: 'Film verwijderen',
      description: 'Hiermee verwijderd een admin een film.',
      scenario: ['Actor klikt op de verwijder knop.',
      'De applicatie geeft waarschuwingsmelding.',
      'Indien geaccepteerd dan redirect de applicatie naar de index pagina en de film wordt verwijderd.'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en minimaal 1 film bestaat.',
      postcondition: 'De film is verwijderd.'
    },
    {
      id: 'UC-16',
      name: 'Voorstelling verwijderen',
      description: 'Hiermee verwijderd een admin een voorstelling.',
      scenario: ['Actor klikt op de verwijder knop.',
      'De applicatie geeft waarschuwingsmelding.',
      'Indien geaccepteerd dan redirect de applicatie naar de index pagina en de voorstelling wordt verwijderd.'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en minimaal 1 voorstelling bestaat.',
      postcondition: 'De voorstelling is verwijderd.'
    },
    {
      id: 'UC-17',
      name: 'Ticket verwijderen',
      description: 'Hiermee verwijderd een admin een ticket.',
      scenario: ['Actor klikt op de verwijder knop.',
      'De applicatie geeft waarschuwingsmelding.',
      'Indien geaccepteerd dan redirect de applicatie naar de index pagina en de ticket wordt verwijderd.'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd en minimaal 1 ticket bestaat.',
      postcondition: 'De ticket is verwijderd.'
    }
  ]

  constructor() {}

  ngOnInit() {}
}
