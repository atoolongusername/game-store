import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TicketListComponent } from './ticket-list/ticket-list.component';
import { TicketDetailComponent } from './ticket-detail/ticket-detail.component';
import { TicketEditComponent } from './ticket-edit/ticket-edit.component';
import { TicketDeleteComponent } from './ticket-delete/ticket-delete.component';
import { AuthGuard } from 'src/app/shared/authguard/auth.guard';
import { FormsModule } from '@angular/forms';
import {LoadingModule} from "../../shared/loading/loading.module";

const routes: Routes = [
  { path: 'tickets', pathMatch: 'full', component: TicketListComponent, canActivate: [AuthGuard] },
  { path: 'tickets/detail/:id', pathMatch: 'full', component: TicketDetailComponent, canActivate: [AuthGuard] },
  { path: 'tickets/create', pathMatch: 'full', component: TicketEditComponent, canActivate: [AuthGuard] },
  { path: 'tickets/:id/edit', pathMatch: 'full', component: TicketEditComponent, canActivate: [AuthGuard] },
  { path: 'tickets/:id/delete', pathMatch: 'full', component: TicketDeleteComponent, canActivate: [AuthGuard] },
];

@NgModule({
  declarations: [TicketListComponent, TicketDetailComponent, TicketEditComponent, TicketDeleteComponent],
  imports: [
    CommonModule, RouterModule.forRoot(routes), FormsModule, LoadingModule
  ],
})
export class TicketModule { }
