import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { delay, switchMap, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/shared/auth.service';
import { User } from '../../user/user.model';
import { Ticket } from '../ticket.model';
import { TicketService } from '../ticket.service';
import {UserService} from "../../user/user.service";

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.css']
})
export class TicketListComponent implements OnInit {
  tickets$: Observable<Ticket[]>;
  users: User[];
  loggedInUser$ = new Observable<User | undefined>();

  constructor(
    private ticketService: TicketService,
    private userService: UserService,
    private route: ActivatedRoute,
    private authService: AuthService
    ) { }

  ngOnInit(): void {
    this.loggedInUser$ = this.authService.currentUser$;

    this.tickets$ = this.route.paramMap.pipe(
      delay(500),
      switchMap(() =>
        this.ticketService.list()
      ),
      tap(console.log)
    );
  }
}
