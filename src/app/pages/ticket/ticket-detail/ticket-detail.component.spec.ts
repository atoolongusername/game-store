import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketDetailComponent } from './ticket-detail.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {Component, NO_ERRORS_SCHEMA} from "@angular/core";

@Component({ selector: 'app-loading', template: '' })
class LoadingComponent {}

@Component({ selector: 'app-alert', template: '' })
class AlertComponent {}

@Component({ selector: 'app-navbar', template: '' })
class NavbarComponent {}

@Component({ selector: 'app-footer', template: '' })
class FooterComponent {}

describe('TicketDetailComponent', () => {
  let component: TicketDetailComponent;
  let fixture: ComponentFixture<TicketDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TicketDetailComponent ],
      imports: [HttpClientTestingModule, RouterTestingModule],
      schemas: [ NO_ERRORS_SCHEMA ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
