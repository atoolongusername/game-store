import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { delay, switchMap, tap } from 'rxjs/operators';
import { Ticket } from '../ticket.model';
import { TicketService } from '../ticket.service';

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.css']
})
export class TicketDetailComponent implements OnInit, OnDestroy {
  ticket: Ticket;
  sub: Subscription;

  constructor(private ticketService: TicketService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.sub = this.route.paramMap.pipe(
      delay(500),
      tap((params: ParamMap) => console.log('id =', params.get('id'))),
      switchMap((params: ParamMap) => {
        return this.ticketService.read(undefined, params.get('id') || '-1');
      }),
      tap(console.log)
    ).subscribe((ticket) => {
      this.ticket = ticket;
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
