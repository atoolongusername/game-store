import { TestBed } from '@angular/core/testing';
import { TicketService } from './ticket.service';
import {Component} from "@angular/core";

@Component({ selector: 'app-loading', template: '' })
class LoadingComponent {}

@Component({ selector: 'app-alert', template: '' })
class AlertComponent {}

@Component({ selector: 'app-navbar', template: '' })
class NavbarComponent {}

@Component({ selector: 'app-footer', template: '' })
class FooterComponent {}
describe('TicketService', () => {
  let service: TicketService;
  let httpSpy: any;
  let alertServiceSpy: any;

  beforeEach(() => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', [
      'error',
      'succes',
    ]);

    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
    service = new TicketService(httpSpy, alertServiceSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
