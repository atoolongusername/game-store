import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { MovieService } from '../../movie/movie.service';
import { Ticket } from '../ticket.model';
import { TicketService } from '../ticket.service';

@Component({
  selector: 'app-ticket-delete',
  templateUrl: './ticket-delete.component.html',
  styleUrls: ['./ticket-delete.component.css']
})
export class TicketDeleteComponent implements OnInit {
  ticket$: Observable<Ticket[]>;

  constructor(private ticketService: TicketService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    console.log('delete ticket');

    this.ticketService
      .delete(undefined, this.route.snapshot.paramMap.get('id') || '-1')
      .subscribe(() =>
      this.router.navigate(['/tickets'], { relativeTo: this.route })
    );
  }

}
