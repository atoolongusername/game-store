import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { of } from 'rxjs';
import { Subscription } from 'rxjs/internal/Subscription';
import { tap, switchMap } from 'rxjs/operators';
import { AuthService } from 'src/app/shared/auth.service';
import { Room } from '../../room/room.model';
import { RoomService } from '../../room/room.service';
import { Show } from '../../show/show.model';
import { ShowService } from '../../show/show.service';
import { User } from '../../user/user.model';
import { UserService } from '../../user/user.service';
import { Ticket } from '../ticket.model';
import { TicketService } from '../ticket.service';

@Component({
  selector: 'app-ticket-edit',
  templateUrl: './ticket-edit.component.html',
  styleUrls: ['./ticket-edit.component.css']
})
export class TicketEditComponent implements OnInit, OnDestroy {
  ticket: Ticket;
  sub: Subscription;
  subShows: Subscription;
  subRooms: Subscription;
  subUsers: Subscription;
  shows: Show[];
  rooms: Room[];
  users: User[];
  currentUserId?: String;

  constructor(
    private ticketService: TicketService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private showService: ShowService,
    private roomService: RoomService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.sub = this.route.paramMap.pipe(
      tap(console.log),
      switchMap((params: ParamMap) => {
        if(!params.get('id')) {
          return of({
            showId: '',
            seatId: '',
            userId: '',
          });
        } else {
          return this.ticketService.read(undefined, params.get('id') || '-1');
        }
      }),
      tap(console.log)
    ).subscribe((ticket) => {
      this.ticket = ticket;
    })

    this.subShows = this.showService.list().subscribe((shows) => (this.shows = shows));
    this.subRooms = this.roomService.list().subscribe((rooms) => (this.rooms = rooms));
    this.subUsers = this.userService.list().subscribe((users) => (this.users = users));

    this.currentUserId = this.authService.currentUser$.value?._id;

  }

  onSubmit(): void {
    console.log('onSubmit', this.ticket);

    const user = this.authService.currentUser$.value;

    if(this.ticket._id) {
      console.log('update ticket');
      this.ticket.updated_by = user?._id;

      this.ticketService
      .update(this.ticket)
      .subscribe(() =>
        this.router.navigate(['/tickets'], { relativeTo: this.route })
      );
    } else {
      console.log('create ticket');
      this.ticket.created_by = user?._id;
      this.ticketService
        .create(this.ticket)
        .subscribe(() =>
          this.router.navigate(['..'], { relativeTo: this.route })
        );
    }
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
