import { Entity } from 'src/app/shared/common/entity.model'
import {User} from "../user/user.model";
import {Show} from "../show/show.model";

export class Ticket extends Entity {
    showId: string
    seatId: string
    userId: string
    created_by?: string;
    updated_by?: string;
    user?: User;
    show?: Show;
}
