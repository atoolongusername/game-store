import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { delay, switchMap, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/shared/auth.service';
import { User } from '../../user/user.model';
import { Movie } from '../movie.model';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {
  movies$: Observable<Movie[]>;
  loggedInUser$ = new Observable<User | undefined>();

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute,
    private authService: AuthService
    ) { }

  ngOnInit(): void {
    this.loggedInUser$ = this.authService.currentUser$;

    this.movies$ = this.route.paramMap.pipe(
      delay(500),
      switchMap(() =>
        this.movieService.list()
      ),
      tap(console.log)
    );
  }

}
