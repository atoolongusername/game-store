import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertService } from 'src/app/shared/alert/alert.service';
import { EntityService } from 'src/app/shared/common/entity-service.model';
import { environment } from 'src/environments/environment';
import { Movie } from './movie.model';

@Injectable({
  providedIn: 'root'
})
export class MovieService extends EntityService<Movie> {
  movies: Movie[];

  constructor(http: HttpClient, public alertService: AlertService) {
    super(http, environment.SERVER_API_URL, 'movies', alertService)
   }
}
