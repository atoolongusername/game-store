import { Entity } from 'src/app/shared/common/entity.model';


export enum Genre {
  action = 'actie',
  horror = 'horror',
  comedy = 'komedie',
  unknown = "onbekend"
}

export class Movie extends Entity {
  name: string;
  image: string;
  age: number;
  releaseDate: string;
  desc: string;
  genre: Genre;
  created_by?: string;
  updated_by?: string;
}
