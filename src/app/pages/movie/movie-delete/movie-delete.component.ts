import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import { delay, switchMap, tap } from 'rxjs/operators';
import { Movie } from '../movie.model';
import { MovieService } from '../movie.service';
import {AuthService} from "../../../shared/auth.service";
import {formatDate} from "@angular/common";
import {AlertService} from "../../../shared/alert/alert.service";

@Component({
  selector: 'app-movie-delete',
  templateUrl: './movie-delete.component.html',
  styleUrls: ['./movie-delete.component.css']
})
export class MovieDeleteComponent implements OnInit, OnDestroy {
  movie: Movie;
  sub: Subscription;

  constructor(private movieService: MovieService, private route: ActivatedRoute, private router: Router, private authService: AuthService, private alertService: AlertService,) { }

  ngOnInit(): void {
    console.log('delete movie');

    const user = this.authService.currentUser$.value;

    this.sub = this.route.paramMap.pipe(
      delay(500),
      tap((params: ParamMap) => console.log('id =', this.route.snapshot.paramMap.get('id'))),
      switchMap((params: ParamMap) => {
        return this.movieService.read(undefined, this.route.snapshot.paramMap.get('id') || '-1');
      }),
      tap(console.log)
    ).subscribe((movie) => {
      this.movie = movie;

      if(movie.created_by != user?._id)
      {
        this.router.navigate(['/movies'], { relativeTo: this.route })
        this.alertService.error('U kunt alleen uw eigen films bewerken.');
        return;
      }

      this.movieService
        .delete(undefined, this.route.snapshot.paramMap.get('id') || '-1')
        .subscribe(() =>
          this.router.navigate(['/movies'], { relativeTo: this.route })
        );
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
