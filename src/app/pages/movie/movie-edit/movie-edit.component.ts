import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, of, Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { Genre, Movie } from '../movie.model';
import { MovieService } from '../movie.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';
import {AlertService} from "../../../shared/alert/alert.service";

@Component({
  selector: 'app-movie-edit',
  templateUrl: './movie-edit.component.html',
  styleUrls: ['./movie-edit.component.css']
})
export class MovieEditComponent implements OnInit, OnDestroy {
  movie: Movie;
  subscription: Subscription;
  genres: any[] = [
    { longname: 'Actie', shortname: 'actie' },
    { longname: 'Horror', shortname: 'horror' },
    { longname: 'Komedie', shortname: 'komedie' }
  ];

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private alertService: AlertService,
  ) { }

  ngOnInit(): void {
    const currentDate = new Date();

    this.subscription = this.route.paramMap.pipe(
      tap(console.log),
      switchMap((params: ParamMap) => {
        if(!params.get('id')) {
          return of({
            name: '',
            image: 'placeholder',
            age: 18,
            releaseDate: currentDate,
            desc: 'Een vet coole film',
            genre: Genre.unknown,
          });
        } else {
          return this.movieService.read(undefined, params.get('id') || '-1');
        }
      }),
      tap(console.log)
    ).subscribe((movie) => {
      this.movie = movie;
      this.movie.name = movie.name !== '' ? movie.name : 'Nieuwe film';
    })
  }

  onSubmit(): void {
    console.log('onSubmit', this.movie);

    const user = this.authService.currentUser$.value;

    if(this.movie._id) {
      console.log('update movie');

      if(this.movie.created_by != user?._id)
      {
        this.router.navigate(['/movies'], { relativeTo: this.route })
        this.alertService.error('U kunt alleen uw eigen films bewerken.');
        return;
      }

      this.movie.updated_by = user?._id;

      this.movieService
      .update(this.movie)
      .subscribe(() =>
        this.router.navigate(['/movies'], { relativeTo: this.route })
      );
    } else {
      console.log('create movie');
      this.movie.created_by = user?._id;

      this.movieService
        .create(this.movie)
        .subscribe(() =>
          this.router.navigate(['..'], { relativeTo: this.route })
        );
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
