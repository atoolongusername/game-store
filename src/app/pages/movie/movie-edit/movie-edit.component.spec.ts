import { formatDate } from '@angular/common';
import {Component, Directive, Input, HostListener, NO_ERRORS_SCHEMA} from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Router, ActivatedRoute, convertToParamMap } from '@angular/router';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { AlertService } from 'src/app/shared/alert/alert.service';
import { AuthService } from 'src/app/shared/auth.service';
import { User } from '../../user/user.model';
import { Genre, Movie } from '../movie.model';
import { MovieService } from '../movie.service';
import { MovieEditComponent } from './movie-edit.component';

@Component({ selector: 'app-loading', template: '' })
class LoadingComponent {}

@Component({ selector: 'app-alert', template: '' })
class AlertComponent {}

@Component({ selector: 'app-navbar', template: '' })
class NavbarComponent {}

@Component({ selector: 'app-footer', template: '' })
class FooterComponent {}

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[routerLink]',
})
export class RouterLinkStubDirective {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  @HostListener('click')
  onClick(): void {
    this.navigatedTo = this.linkParams;
  }
}

// Global mock objects
const expectedMovie: Movie = {
  _id: undefined,
  name: 'title',
  image: 'placeholder.png',
  age: 18,
  releaseDate: formatDate(new Date(), 'dd-MM-yyyy hh:mm', 'en-US'),
  desc: 'desc',
  genre: Genre.comedy,
  created_by: undefined,
  updated_by: undefined
};

const expectedUserData: User = {
  _id: undefined,
  name: 'Firstname',
  lastName: 'Lastname',
  password: 'password',
  birthdate: formatDate(new Date(), 'dd-MM-yyyy hh:mm', 'en-US'),
  email: 'user@host.com',
  phone: '068045034',
  isAdmin: false,
  token: 'some.dummy.token'
};

/**
 *
 */
describe('MovieEditComponent', () => {
  let component: MovieEditComponent;
  let fixture: ComponentFixture<MovieEditComponent>;

  let alertServiceSpy;
  let movieServiceSpy = jasmine.createSpyObj('MovieService', [
    'list',
    'read',
    'update',
  ]);
  let authServiceSpy;
  let routerSpy = jasmine.createSpyObj('Router', ['navigateByUr']);

  /**
   *
   */
  beforeEach(() => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', [
      'error',
      'success',
    ]);
    authServiceSpy = jasmine.createSpyObj(
      'AuthService',
      [
        'login',
        'register',
        'logout',
        'getUserFromLocalStorage',
        'saveUserToLocalStorage'
      ]
      // ['currentUser$']
    );
    const mockUser$ = new BehaviorSubject<User>(expectedUserData);
    authServiceSpy.currentUser$ = mockUser$;

    movieServiceSpy = jasmine.createSpyObj('MovieService', ['read', 'update']);
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

    TestBed.configureTestingModule({
      // The declared components needed to test the UsersComponent.
      declarations: [
        MovieEditComponent, // The 'real' component that we will test
        RouterLinkStubDirective, // Stubbed component required to instantiate the real component.
        LoadingComponent,
        AlertComponent,
        NavbarComponent,
        FooterComponent
      ],
      imports: [FormsModule],
      schemas: [ NO_ERRORS_SCHEMA ],
      //
      // The constructor of our real component uses dependency injected services
      // Never provide the real service in testcases!
      //
      providers: [
        { provide: AuthService, useValue: authServiceSpy },
        { provide: AlertService, useValue: alertServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: MovieService, useValue: movieServiceSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(
              convertToParamMap({
                id: 1,
              })
            ),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MovieEditComponent);
    component = fixture.componentInstance;
  });

  /**
   *
   */
  afterEach(() => {
    fixture.destroy();
  });

  /**
   *
   */
  it('should create', (done) => {
    movieServiceSpy.read.and.returnValue(of(expectedMovie));

    // Deze zijn nodig zodat we in ngOnDestroy kunnen unsubsciben.
    component.subscription = new Subscription();

    fixture.detectChanges();
    expect(component).toBeTruthy();
    //expect(component.debug).toEqual(false);

    setTimeout(() => {
      expect(component.movie).toEqual(expectedMovie);
      done();
    }, 200);
  });

  it('should get movie', () => {
    movieServiceSpy.read.and.returnValue(of(expectedMovie));
    fixture.detectChanges();

    expect(component.movie).toEqual(expectedMovie);
    expect(component.movie.name).toEqual(expectedMovie.name);
  });

  it('should update movie', () => {
    movieServiceSpy.read.and.returnValue(of(expectedMovie));
    fixture.detectChanges();
    movieServiceSpy.update.and.returnValue(of(expectedMovie));
    fixture.detectChanges();

    expect(component.movie).toEqual(expectedMovie);
    expect(component.movie.name).toEqual(expectedMovie.name);
  });
});
