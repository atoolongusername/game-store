import { formatDate } from '@angular/common';
import { of, throwError } from 'rxjs';
import { AlertService } from 'src/app/shared/alert/alert.service';
import { Genre, Movie } from './movie.model';
import { MovieService } from './movie.service';
import {Component} from "@angular/core";

@Component({ selector: 'app-loading', template: '' })
class LoadingComponent {}

@Component({ selector: 'app-alert', template: '' })
class AlertComponent {}

@Component({ selector: 'app-navbar', template: '' })
class NavbarComponent {}

@Component({ selector: 'app-footer', template: '' })
class FooterComponent {}

describe('MovieService', () => {
  let service: MovieService;

  let httpSpy: any;
  let alertServiceSpy: any;
  let movieServiceSpy: any;

  const expectedData: Movie = {
    _id: '1',
    name: 'title',
    image: 'placeholder.png',
    age: 18,
    releaseDate: formatDate(new Date(), 'dd-MM-yyyy hh:mm', 'en-US'),
    desc: 'desc',
    genre: Genre.comedy,
    created_by: undefined,
    updated_by: undefined
  }

  const expectedUpdatedData: Movie = {
    _id: '1',
    name: 'updated-title',
    image: 'placeholder.png',
    age: 18,
    releaseDate: formatDate(new Date(), 'dd-MM-yyyy hh:mm', 'en-US'),
    desc: 'desc',
    genre: Genre.comedy,
    created_by: undefined,
    updated_by: undefined
  }

  beforeEach(() => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', [
      'error',
      'succes'
    ]);

    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

    movieServiceSpy = jasmine.createSpyObj('MovieService', [
      'list',
      'read',
      'update',
      'create',
      'delete'
    ]);

    service = new MovieService(httpSpy, alertServiceSpy);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should create a movie with valid movie information', () => {
    // Set input and expected output
    const movie = new Movie('');

    movie._id = '1'
    movie.name = 'title';
    movie.image = 'placeholder.png';
    movie.age = 18;
    movie.releaseDate = formatDate(new Date(), 'dd-MM-yyyy hh:mm', 'en-US');
    movie.desc = 'desc';
    movie.genre = Genre.comedy;

    httpSpy.post.and.returnValue(of(expectedData));
    httpSpy.get.and.returnValue(of(expectedData));

    const subs = service.create(expectedData).subscribe((movie) => {
      expect(movie?.name).toEqual('title');
    });

    subs.unsubscribe();
  });

  /**
   *
   */
  it('should NOT create a movie with invalid user information', () => {
    const movie = new Movie('');
    movie.name = "title";

    const expectedErrorResponse = {
      error: { message: '"Movie validation failed: genre: A movie needs to have a genre., desc: A movie needs to have a description., releaseDate: A movie needs to have a release date., age: A movie needs to have an age., image: A movie needs to have an image., name: A movie needs to have a name."' },
      name: 'HttpErrorResponse',
      ok: false,
      status: 400,
      statusText: 'Bad Request',
    };

    httpSpy.post.and.returnValue(throwError(expectedErrorResponse));

    const subs = service.create(movie).subscribe((movie) => {
      expect(movie).toBe(undefined);
    });

    subs.unsubscribe();
  });

  it('should update a movie with valid movie information', () => {
    const movie = new Movie('');

    movie._id = '1'
    movie.name = 'updated-title';
    movie.image = 'placeholder.png';
    movie.age = 18;
    movie.releaseDate = formatDate(new Date(), 'dd-MM-yyyy hh:mm', 'en-US');
    movie.desc = 'desc';
    movie.genre = Genre.comedy;

    httpSpy.put.and.returnValue(of(expectedUpdatedData));
    httpSpy.get.and.returnValue(of(expectedUpdatedData));

    const subs = service.update(expectedUpdatedData).subscribe((movie) => {
      expect(movie?.name).toEqual('updated-title');
    });

    subs.unsubscribe();
  });

  /**
   *
   */
  it('should NOT update a movie with invalid movie information', () => {
    const movie = new Movie('');
    movie._id = '2';

    const expectedErrorResponse = {
      error: { message: '"Invalid resource id: 2"' },
      name: 'HttpErrorResponse',
      ok: false,
      status: 400,
      statusText: 'Bad Request',
    };

    httpSpy.put.and.returnValue(throwError(expectedErrorResponse));

    const subs = service.update(movie).subscribe((movie) => {
      expect(movie).toBe(undefined);
    });

    subs.unsubscribe();
  });

  it('should delete a movie', () => {
    const movie = new Movie('');

    movie._id = '1'
    httpSpy.delete.and.returnValue(of(expectedData));

    const subs = service.delete(undefined, movie._id).subscribe((movie) => {
      expect(movie?._id).toEqual(movie?._id);
    });

    subs.unsubscribe();
  });
});
