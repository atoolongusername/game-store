import { formatDate } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { delay, switchMap, tap } from 'rxjs/operators';
import { Movie } from '../movie.model';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit, OnDestroy {
  movie: Movie;
  sub: Subscription;

  constructor(private movieService: MovieService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.sub = this.route.paramMap.pipe(
      delay(500),
      tap((params: ParamMap) => console.log('id =', params.get('id'))),
      switchMap((params: ParamMap) => {
        return this.movieService.read(undefined, params.get('id') || '-1');
      }),
      tap(console.log)
    ).subscribe((movie) => {
      this.movie = movie;

      if(movie.releaseDate != null)
        this.movie.releaseDate = formatDate(movie.releaseDate, 'dd-MM-yyyy', 'en-US');
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
