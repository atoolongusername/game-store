import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MovieListComponent } from './movie-list/movie-list.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { MovieEditComponent } from './movie-edit/movie-edit.component';
import { FormsModule } from '@angular/forms';
import { MovieDeleteComponent } from './movie-delete/movie-delete.component';
import { AuthGuard } from 'src/app/shared/authguard/auth.guard';
import {LoadingModule} from "../../shared/loading/loading.module";

const routes: Routes = [
  { path: 'movies', pathMatch: 'full', component: MovieListComponent },
  { path: 'movies/detail/:id', pathMatch: 'full', component: MovieDetailComponent },
  { path: 'movies/create', pathMatch: 'full', component: MovieEditComponent, canActivate: [AuthGuard] },
  { path: 'movies/:id/edit', pathMatch: 'full', component: MovieEditComponent, canActivate: [AuthGuard] },
  { path: 'movies/:id/delete', pathMatch: 'full', component: MovieDeleteComponent, canActivate: [AuthGuard] },
];

@NgModule({
  declarations: [MovieListComponent, MovieDetailComponent, MovieEditComponent, MovieDeleteComponent],
  imports: [
    CommonModule, RouterModule.forRoot(routes), FormsModule, LoadingModule
  ]
})
export class MovieModule { }
