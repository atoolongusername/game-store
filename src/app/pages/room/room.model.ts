import { Entity } from 'src/app/shared/common/entity.model';

export class Room extends Entity {
  name: string;
  capacity: number;
}
