import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { Room } from '../room.model';
import { RoomService } from '../room.service';
import { delay, switchMap, tap } from "rxjs/operators";

@Component({
  selector: 'app-room-detail',
  templateUrl: './room-detail.component.html',
  styleUrls: ['./room-detail.component.css']
})
export class RoomDetailComponent implements OnInit, OnDestroy {
  room: Room;
  sub: Subscription;

  constructor(private roomService: RoomService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.sub = this.route.paramMap.pipe(
      delay(500),
      tap((params: ParamMap) => console.log('id =', params.get('id'))),
      switchMap((params: ParamMap) => {
        return this.roomService.read(undefined, params.get('id') || '-1');
      }),
      tap(console.log)
    ).subscribe((room) => {
      this.room = room;
    });
  }

 ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
