import { Injectable } from '@angular/core';
import { Room } from './room.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AlertService } from 'src/app/shared/alert/alert.service';
import { EntityService } from 'src/app/shared/common/entity-service.model';

@Injectable({
  providedIn: 'root'
})
export class RoomService extends EntityService<Room> {
  rooms: Room[];

  constructor(http: HttpClient, public alertService: AlertService) {
    super(http, environment.SERVER_API_URL, 'rooms', alertService)
   }
}
