import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RoomListComponent } from './room-list/room-list.component';
import { RoomDetailComponent } from './room-detail/room-detail.component';
import { RoomEditComponent } from './room-edit/room-edit.component';
import {LoadingModule} from "../../shared/loading/loading.module";

const routes: Routes = [
  { path: 'rooms', pathMatch: 'full', component: RoomListComponent },
  { path: 'rooms/detail/:id', pathMatch: 'full', component: RoomDetailComponent },
];

@NgModule({
  declarations: [RoomListComponent, RoomDetailComponent, RoomEditComponent],
  imports: [
    CommonModule, RouterModule.forRoot(routes), LoadingModule
  ]
})
export class RoomModule { }
