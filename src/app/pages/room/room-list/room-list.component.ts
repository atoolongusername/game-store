import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { delay, switchMap, tap } from 'rxjs/operators';
import { AuthService } from 'src/app/shared/auth.service';
import { User } from '../../user/user.model';
import { Room } from '../room.model';
import { RoomService } from '../room.service';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent implements OnInit {
  rooms$: Observable<Room[]>;
  loggedInUser$ = new Observable<User | undefined>();

  constructor(
    private roomService: RoomService,
    private route: ActivatedRoute,
    private authService: AuthService
    ) { }

  ngOnInit(): void {
    this.loggedInUser$ = this.authService.currentUser$;

    this.rooms$ = this.route.paramMap.pipe(
      delay(500),
      switchMap(() =>
        this.roomService.list()
      ),
      tap(console.log)
    );
  }

}
