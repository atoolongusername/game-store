import { Entity } from './entity.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { EMPTY, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AlertService } from '../alert/alert.service';

export abstract class EntityService<T extends Entity> {
  constructor(
    public readonly http: HttpClient,
    public readonly url: string,
    public readonly endpoint: string,
    public alertService = new AlertService()
  ) { }

  public list(params?: HttpParams): Observable<T[]>
  {
    const endpoint = `${this.url}${this.endpoint}`
    console.log(`list ${endpoint} (params = ${params})`);

      return this.http
        .get<T[]>(endpoint).pipe(
        catchError( error => {
        console.log(error);
        this.alertService.error(error.error.message || error.message);
        return EMPTY;
      })
    );
  }

  public read(params?: HttpParams, id?: string): Observable<T | undefined>
  {
    const endpoint = `${this.url}${this.endpoint}/${id}`
    console.log(`read ${endpoint} (params = ${params})`);

      return this.http
        .get<T>(endpoint).pipe(
        catchError( error => {
        console.log(error);
        this.alertService.error(error.error.message || error.message);
        return EMPTY;
      })
    );
  }

  public update(params?: Entity): Observable<T | undefined>
  {
    const endpoint = `${this.url}${this.endpoint}/${params?._id}`
    console.log(`update ${endpoint} (params = ${params})`);

    return this.http
        .put<T>(endpoint, params).pipe(
        catchError( error => {
        console.log(error);
        this.alertService.error(error.error.message || error.message);
        return EMPTY;
      })
    );
  }

  public create(params?: Entity): Observable<T | undefined>
  {
    const endpoint = `${this.url}${this.endpoint}`
    console.log(`create ${endpoint} (params = ${params})`);

    return this.http
        .post<T>(endpoint, params).pipe(
        catchError( error => {
        console.log(error);
        this.alertService.error(error.error.message || error.message);
        return EMPTY;
      })
    );
  }

  public delete(params?: HttpParams, id?: string): Observable<T | undefined>
  {
    const endpoint = `${this.url}${this.endpoint}/${id}`
    console.log(`delete ${endpoint} (params = ${params})`);

    return this.http
        .delete<T>(endpoint).pipe(
        catchError( error => {
        console.log(error);
        this.alertService.error(error.error.message || error.message);
        return EMPTY;
      })
    );
  }
}
