import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/pages/user/user.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User;
  submitted = false;

  constructor(
    private authService: AuthService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.user = new User('');
  }

  onSubmit(): void {
    this.submitted = true;

    if (this.user.email && this.user.password) {
      const email     = this.user.email;
      const password  = this.user.password;

      this.authService.login(email, password).subscribe((user) => {
        if(user) {
          console.log('user = ', user);
          this.router.navigate(['/']);
        }
      });
    } else {
      console.error('Email or password is invalid');
    }
  }

}
