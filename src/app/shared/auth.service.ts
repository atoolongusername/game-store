import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, EMPTY, Observable, of, Subscription } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../pages/user/user.model';
import { UserService } from '../pages/user/user.service';
import { AlertService } from './alert/alert.service';
import { Entity } from './common/entity.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnDestroy {
  public endPoint = "auth/";

  user: User;
  currentUser?: User;
  subCurrentUser: Subscription;
  loggedInUser$ = new Observable<User | undefined>();

  public currentUser$ = new BehaviorSubject<User | undefined>(undefined);
  private readonly CURRENT_USER = 'currentuser';
  private readonly headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  constructor(
    private readonly http: HttpClient,
    private alertService: AlertService,
    private route: ActivatedRoute,
    private router: Router,
    ) { }

  login(email: string, password: string): Observable<User | undefined> {
    console.log(`login at ${environment.SERVER_API_URL}${this.endPoint}login`);

    return this.http.post(
        `${environment.SERVER_API_URL}${this.endPoint}login`,
        { email, password },
        { headers: this.headers }
      ).pipe(map((response: any) => {
        const user = { ...response } as User;
        this.saveUserToLocalStorage(user);
        this.currentUser$.next(user);
        this.alertService.success('Gebruiker is ingelogd!');
        return user;
      }), catchError((error: any) => {
        this.alertService.error(error.error.message || error.message);
        return of(undefined);
      }));
  }

  register(params?: Entity): Observable<User | undefined> {
    const endPoint = `${environment.SERVER_API_URL}${this.endPoint}register`
    console.log(`Regist ${endPoint} (params = ${params})`);

    return this.http
        .post<User>(endPoint, params).pipe(map((response: any) => {
        const user = { ...response } as User;
        this.alertService.info('Gebruiker is geregistreerd!');
        return user;
      }), catchError( error => {
        this.alertService.error(error.error.message || error.message);
        return of(undefined);
      })
    );
  }

  follow(userId?: string): Observable<User | undefined> {
    const endPoint = `${environment.SERVER_API_URL}users/${this.currentUser$.value?._id}`

    console.log(`Follow ${endPoint} (params = ${this.currentUser})`);

    this.currentUser$.value?.friends?.push(userId!);

    const user = new User(this.currentUser$.value);
    user.email = this.currentUser$.value?.email!;
    user.birthdate = this.currentUser$.value?.birthdate!;
    user.isAdmin = this.currentUser$.value?.isAdmin!
    user.lastName = this.currentUser$.value?.lastName!
    user.name = this.currentUser$.value?.name!
    user.phone = this.currentUser$.value?.phone!
    user.token = this.currentUser$.value?.token!
    user.friends = this.currentUser$.value?.friends!;

    return this.http
        .put<User>(endPoint, user).pipe(
        tap(data => console.log(data)),
        catchError( error => {
        console.log(error);
        return EMPTY;
      })
    );
  }

  logout() {
    const localUser = JSON.parse(localStorage.getItem(this.CURRENT_USER) || '{}');
    console.log(`logging out user #${localUser._id} ${localUser.name} ${localUser.lastName}`);

    localStorage.removeItem(this.CURRENT_USER);
    this.currentUser$.next(undefined);

    this.router.navigate(['/login'], { relativeTo: this.route })
  }

  getUserFromLocalStorage(): Observable<User> {
    const localUser = JSON.parse(localStorage.getItem(this.CURRENT_USER) || '{}');
    console.log('localuser', localUser['entity']);
    return of(localUser);
  }

  private saveUserToLocalStorage(user: User): void {
    localStorage.setItem(this.CURRENT_USER, JSON.stringify(user));
  }


  ngOnDestroy(): void {
    this.subCurrentUser.unsubscribe();
  }
}
