import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/pages/user/user.model';
import { AlertService } from '../alert/alert.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user: User;
  submitted = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {
    const currentDate = new Date();
    const currentDateFormatted = formatDate(currentDate, 'dd-MM-yyyy', 'en-US');

    this.user = new User('');
    this.user.birthdate = currentDateFormatted;
  }

  onSubmit(): void {
    this.submitted = true;

    console.log('onSubmit', this.user);

    this.authService
      .register(this.user)
      .subscribe(() => {
          if (this.user.email && this.user.password && this.user._id) {
            const email     = this.user.email;
            const password  = this.user.password;

            this.authService.login(email, password).subscribe((user) => {
              if(user) {
                console.log('user = ', user);
                this.router.navigate(['/']);
              }
            });
          }
        }
      );
  }

}
