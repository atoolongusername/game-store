import { Component, OnInit } from '@angular/core';
import { Alert, AlertService } from './alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  subscription: any;
  alert: Alert;
  staticAlertClosed: boolean;

  constructor(private alertService: AlertService) { }

  ngOnInit(): void {
    this.subscription = this.alertService.alert$.subscribe((alert) => {
      this.alert = alert;
      this.staticAlertClosed = false;

      setTimeout(() => (this.staticAlertClosed = true), 6000);
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
