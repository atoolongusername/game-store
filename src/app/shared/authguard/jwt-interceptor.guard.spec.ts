import { TestBed } from '@angular/core/testing';

import { JwtInterceptor } from './jwt-interceptor';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {NO_ERRORS_SCHEMA} from "@angular/core";

describe('JwtInterceptorGuard', () => {
  let guard: JwtInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({imports: [HttpClientTestingModule, RouterTestingModule],  schemas: [ NO_ERRORS_SCHEMA ],});
    guard = TestBed.inject(JwtInterceptor);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
