import { Component, OnDestroy, OnInit } from '@angular/core';
import { faTicketAlt } from '@fortawesome/free-solid-svg-icons';
import { faFilm } from '@fortawesome/free-solid-svg-icons';
import { faHome } from '@fortawesome/free-solid-svg-icons';
import { faChair } from '@fortawesome/free-solid-svg-icons';
import { faUserMd } from '@fortawesome/free-solid-svg-icons';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { faCalendarTimes } from '@fortawesome/free-solid-svg-icons';
import { faSignInAlt } from '@fortawesome/free-solid-svg-icons';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/pages/user/user.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {
  loggedInUser$ = new Observable<User | undefined>();
  currentUserId?: String;
  subNavBar: Subscription;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.loggedInUser$ = this.authService.currentUser$;
    this.subNavBar = this.loggedInUser$.subscribe((currentUser) => (this.currentUserId = currentUser?._id));
  }

  collapsed = true;

  toggleCollapsed(): void {
    this.collapsed = !this.collapsed;
  }

  logout(): void {
      this.authService.logout();
  }

  ngOnDestroy(): void {
    this.subNavBar.unsubscribe();
  }

  faTicketAlt = faTicketAlt;
  faFilm = faFilm;
  faHome = faHome;
  faChair = faChair;
  faUserMd = faUserMd;
  faInfoCircle = faInfoCircle;
  faCalendarTimes = faCalendarTimes;
  faSignInAlt = faSignInAlt;
  faSignOutAlt = faSignOutAlt;
  faUser = faUser;
}
