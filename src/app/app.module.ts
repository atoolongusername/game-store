import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { LayoutComponent } from './layout/layout/layout.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AboutComponent } from './pages/about/about.component';
import { UsecaseComponent } from './pages/about/usecase/usecase.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MovieModule } from './pages/movie/movie.module';
import { RoomModule } from './pages/room/room.module';
import { ShowModule } from './pages/show/show.module';
import { TicketModule } from './pages/ticket/ticket.module';
import { UserModule } from './pages/user/user.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthModule } from './shared/auth.module';
import { JwtInterceptor } from './shared/authguard/jwt-interceptor';
import {AlertComponent} from "./shared/alert/alert.component";

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    LayoutComponent,
    DashboardComponent,
    AboutComponent,
    UsecaseComponent,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    MovieModule,
    RoomModule,
    ShowModule,
    TicketModule,
    UserModule,
    AuthModule,
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule,
    HttpClientModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },],
  bootstrap: [AppComponent]
})
export class AppModule { }
