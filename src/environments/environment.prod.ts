export const environment = {
  production: true,
  SERVER_API_URL: 'https://cinamaniacs.herokuapp.com/api/',
};
